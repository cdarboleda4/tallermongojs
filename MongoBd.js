use 
db.createCollection("profesores")
db.profesores.insertMany([
    {
        nombre:'Andrea',
        apellido: 'Cotame',
        correo:'Andre123@gmail.com',
        documento:'10026548',
    },
    {
        nombre:'Samuel',
        apellido: 'Rojas',
        correo:'Samuel@gmail.com',
        documento:'1024578963',
    },
    {
        nombre:'Laura',
        apellido: 'Hernandez',
        correo:'Hernandez123@gmail.com',
        documento:'3107200250',
    }
])
db.createCollection("cursos")
db.cursos.insertMany([
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13341"),
        nombre:'1001',
        creditos: '20',

    },
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13342"),
        nombre:'1002',
        creditos: '22',
    },
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13343"),
        nombre:'1003',
        creditos: '30',
    },
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13344"),
        nombre:'1004',
        creditos: '34',
    }
    
])
db.createCollection("estudiantes")
db.estudiantes.insertMany([
    {
        nombre:'Juan',
        apellido: 'Gonzales',
        correo:'Andrew@gmail.com',
        documento:'105446416',
        curso: new ObjectId("5fce8901c78d5d3387f13364"),

    },
    {
        nombre:'Klerth',
        apellido: 'Ojeda',
        correo:'ojeda123@gmail.com',
        documento:'221002566',
        curso: new ObjectId("5fce8901c78d5d3387f13364"),
    },
    {
        nombre:'Cristian',
        apellido: 'Arboleda',
        correo:'Arboleda@gmail.com',
        documento:'13236463113',
        curso: new ObjectId("5fce8901c78d5d3387f13364"),
    },
    {
        nombre:'Michel',
        apellido: 'Velez',
        correo:'Michel@gmail.com',
        documento:'325565344',
        curso: new ObjectId("5fce8901c78d5d3387f13365"),
    },
    {
        nombre:'Camilo',
        apellido: 'Curtua',
        correo:'curtua10@gmail.com',
        documento:'464632',
        curso: new ObjectId("5fce8901c78d5d3387f13365"),
    },
    {
        nombre:'Dynaluz',
        apellido: 'Ramirez',
        correo:'Dy123345r43@gmail.com',
        documento:'10003234663',
        curso: new ObjectId("5fce8901c78d5d3387f13365"),
    },
    {
        nombre:'Angie',
        apellido: 'Beltran',
        correo:'beltran1213@gmail.com',
        documento:'151515463',
        curso: new ObjectId("5fce8901c78d5d3387f13366"),
    },
    {
        nombre:'Tania',
        apellido: 'Ortiz',
        correo:'Tania@gmail.com',
        documento:'979464141',
        curso: new ObjectId("5fce8901c78d5d3387f13366"),
    },
    {
        nombre:'Santiago',
        apellido: 'Ravelo',
        correo:'ravelo123456@gmail.com',
        documento:'15165420',
        curso: new ObjectId("5fce8901c78d5d3387f13366"),
    },
    {
        nombre:'Steven',
        apellido: 'Castillo',
        correo:'warrior39@gmail.com',
        documento:'78457842',
        curso: new ObjectId("5fce8901c78d5d3387f13367"),
    }
])

//listar estudiantes

db.estudiantes.find().pretty()

//listar profesores

db.profesores.find().pretty()

//listar cursos

db.cursos.find().pretty()

// listar cursos de un profesor 

db.profesores.aggregate(
    [
        {
            $match: {
                _id: ObjectId("5fce6a17c78d5d3387f13341")
            }
        },
        {
            $lookup: {
                from:'cursos',
                localField:'_id',
                foreignField:'profesor',
                as:'cursos',

            }
        }

    ]

).pretty()

//listar los estudiantes de un curso

db.cursos.aggregate(
    [
        {
            $match: {
                _id: ObjectId("5fce8901c78d5d3387f13364")
            }
        },
        {
            $lookup: {
                from:'estudiantes',
                localField:'_id',
                foreignField:'curso',
                as:'estudiantes',

            }
        }

    ]

).pretty()

// listar los cursos de un profesor con sus estudiantes

db.profesores.aggregate(
    [
        {
            $match: {
                _id: ObjectId("5fce6a17c78d5d3387f13341")
            }
        },
        {
            $lookup: {
                from:'cursos',
                localField:'_id',
                foreignField:'profesor',
                as:'cursos',

            }
        },
        {
            $unwind: '$cursos'
        },
        {
            $lookup: {
                from: 'estudiantes',
                localField:'cursos._id',
                foreignField: 'curso',
                as: 'estudiantes' 
            }
        }

    ]

).pretty()